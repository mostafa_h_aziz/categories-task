<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\data\Pagination;
use app\models\Category;
class CategoryController extends Controller
{
    public function actionIndex()
    {
        $categories = $this->get_categories();
        return $this->render('index', [
            'data' => $categories
        ]);
    }


    public function get_categories($parent = 0)
    {
        $html = '<ul>';
        $query = "SELECT * FROM `category` WHERE `parent_id` = '$parent'";
        $categories = Yii::$app->db->createCommand($query)->queryAll();
        foreach($categories as $category)
        {
            $current_id = $category['id'];
            $html .= '<li><input type="button" value="+" id="'.$category['id'].'" class="create_category">' . $category['name'];
            $has_sub = NULL;
            $has_sub = "SELECT COUNT(`parent_id`) FROM `category` WHERE `parent_id` = '$current_id'";
            $has_sub = Yii::$app->db->createCommand($has_sub)->queryAll();
            if(is_array($has_sub))
            {
                $html .= $this->get_categories($current_id);
            }
            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }

    
    public function actionAjax()
    {
        $parent_id = Yii::$app->request->post('parent_id');
        $category = new Category();
        $category->parent_id = $parent_id;
        $category_parent = Category::find()->where(['id'=>$parent_id])->one();
        if($category_parent->parent_id != 0){
            $count = Category::find()->where(['parent_id' => $parent_id])->count();
            preg_match_all('!\d+!', $category_parent->name , $matches);
            $count+= 1;
            if(is_array($matches))
                $category->name = 'SUB '.$category_parent->name.'-'.$count;
            else
            $category->name = 'SUB '.$category_parent->name.'1';

         } else{
            //die('in');
            $count = Category::find()->where(['parent_id' => $parent_id])->count();
            $count+= 1;
            $category->name = 'SUB '.$category_parent->name.$count;
         }
            
        $category->save();
    }


}
