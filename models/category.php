<?php

namespace app\models;

use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public function getChildren()

    {

        return $this->hasMany(Category::className(), ['parent_id' => 'id']);

    }


    public function getParent()

    {

        return $this->hasOne(Category::className(), ['id' => 'parent_id']);

    }


    public function getUrl()

    {

        return url(['listing/index', 'category' => $this->name, 'id' => $this->id]);

    }
}